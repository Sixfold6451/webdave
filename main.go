package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"golang.org/x/net/webdav"
)

var dir string

func main() {

	dirFlag := flag.String("d", "./", "Directory to serve from. Default is CWD")
	httpPort := flag.Int("p", 8083, "Port to serve on")
	username := flag.String("u", "user", "Username to access the files")
	password := flag.String("pwd", "password", "Password to access the files")

	flag.Parse()

	dir = *dirFlag

	srv := &webdav.Handler{
		FileSystem: webdav.Dir(dir),
		LockSystem: webdav.NewMemLS(),
		Logger: func(r *http.Request, err error) {
			if err != nil {
				log.Printf("WEBDAV [%s]: %s, ERROR: %s\n", r.Method, r.URL, err)
			} else {
				log.Printf("WEBDAV [%s]: %s \n", r.Method, r.URL)
			}
		},
	}
	authSrv := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		enteredUsername, enteredPassword, ok := r.BasicAuth()
		if !ok {
			w.Header().Set("WWW-Authenticate", "Basic Realm=\"Restricted Area\"")
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		if enteredUsername != *username || enteredPassword != *password {
			w.Header().Set("WWW-Authenticate", "Basic Realm=\"Restricted Area\"")
			w.WriteHeader(http.StatusForbidden)
			return
		}
		srv.ServeHTTP(w,r)
	})
	http.Handle("/", authSrv)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", *httpPort), nil); err != nil {
		log.Fatalf("Error with WebDAV server: %v", err)
	}

}

